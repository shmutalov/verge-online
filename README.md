# verge-online

Verge Online is MMORPG written in Rust

# License

`verge-online` is distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See [`LICENSE-APACHE`](LICENSE-APACHE) and [`LICENSE-MIT`](LICENSE-MIT) for details.
