use ggez::*;
use ggez::nalgebra as na;
use ggez::{Context, GameResult};
use std::time::Duration;

use crate::managers::*;
use event::{KeyCode, KeyMods, MouseButton};
use input::mouse::button_pressed;

pub struct GameState {
    current_level: Level,
    cursor_pos: na::Point2<f32>,
}

impl GameState {
    pub fn new(ctx: &mut Context, level_name: &str) -> GameResult<GameState> {
        let level = LevelManager::load_level(ctx, level_name)?;

        Ok(GameState {
            current_level: level,
            cursor_pos: na::Point2::new(0.0, 0.0),
        })
    }

    /// handle tap command
    fn tapped(&mut self, x: f32, y: f32) {
        println!("[{}x, {}y] tapped", x, y);
    }

    /// move cursor
    fn move_cursor(&mut self, x: f32, y: f32) {
        println!("[{}x, {}y] moved", x, y);

        self.cursor_pos.x += x;
        self.cursor_pos.y += y;
    }

    /// update cursor position
    fn update_cursor(&mut self, x: f32, y: f32) {
        println!("[{}x, {}y] updated", x, y);

        self.cursor_pos.x = x;
        self.cursor_pos.y = y;
    }
}

impl event::EventHandler for GameState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::WHITE);

        // draw here

        // draw tilemap first
        self.current_level.draw(ctx)?;

        graphics::present(ctx)?;
        timer::sleep(Duration::from_secs(0));
        Ok(())
    }

    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {
        let (delta_x, delta_y) = match keycode {
            KeyCode::Left => (1.0f32, 0.0f32),
            KeyCode::Right => (-1f32, 0f32),
            KeyCode::Up => (0.0f32, 1f32),
            KeyCode::Down => (0f32, -1f32),
            _ => (0f32, 0f32),
        };

        self.move_cursor(delta_x, delta_y);
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        _keycode: KeyCode,
        _keymod: KeyMods
    ) {
    }

    fn mouse_motion_event(&mut self, _ctx: &mut Context, x: f32, y: f32, _xrel: f32, _yrel: f32) {
        if button_pressed(_ctx, MouseButton::Left)
            || button_pressed(_ctx, MouseButton::Right)
            || button_pressed(_ctx, MouseButton::Middle)
        {
            return;
        }

        self.update_cursor(x, y);
    }

    fn mouse_button_down_event(&mut self, _ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        if button == MouseButton::Left {
            self.tapped(x, y);
        }
    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        _button: MouseButton,
        _x: f32,
        _y: f32,
    ) {
    }
}
