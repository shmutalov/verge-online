use ggez::*;
use ggez::{Context, GameResult};
use std::time::Duration;

use event::{KeyCode, KeyMods, MouseButton};

pub struct MainMenuState {}

impl MainMenuState {
    pub fn new(_ctx: &mut Context) -> GameResult<MainMenuState> {
        Ok(MainMenuState {})
    }
}

impl event::EventHandler for MainMenuState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::WHITE);

        // draw here

        graphics::present(ctx)?;
        timer::sleep(Duration::from_secs(0));
        Ok(())
    }

    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        _keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {

    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        _keycode: KeyCode,
        _keymod: KeyMods
    ) {

    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        _button: MouseButton,
        _x: f32,
        _y: f32,
    ) {

    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        _button: event::MouseButton,
        _x: f32,
        _y: f32,
    ) {

    }
}
