extern crate tiled;

use ggez::graphics::spritebatch::*;
use ggez::*;
use ggez::{Context, GameError, GameResult};
use ggez::filesystem::open;

use self::tiled::{Map, Tileset};

use std::error::Error;

pub struct Level {
	/// Tilemap name
	pub name: String,

	/// Tiled map object
	map: Map,

	spritebatch: SpriteBatch,
}

impl Level {
	pub fn new(ctx: &mut Context, name: &str, map: Map) -> GameResult<Level> {
		let spritebatch = Level::init_spritebatch(ctx, &map)?;

		Ok(Level {
			name: name.into(),
			map: map,
			spritebatch: spritebatch,
		})
	}

	/// Inits spritebatch and fills it with geometry
	fn init_spritebatch(ctx: &mut Context, map: &Map) -> GameResult<SpriteBatch> {
		let first_tileset = &map.tilesets[0];
		let first_image = Level::get_image(&first_tileset);

		let width = map.width;
		let height = map.height;
		let tile_width = first_tileset.tile_width;
		let tile_height = first_tileset.tile_height;
		let image_width = first_tileset.images[0].width as u32;
		let image_height = first_tileset.images[0].height as u32;

		let image = graphics::Image::new(ctx, first_image)?;
		let mut spritebatch = SpriteBatch::new(image);

		for ref layer in map.layers.iter() {
			if !layer.visible {
				continue;
			}

			for x in 0..width {
				for y in 0..height {
					let tile = layer.tiles[y as usize][x as usize];
					if tile == 0 {
						continue;
					}
					let tile = tile - 1;

					// rect of the particular tile in the tilesheet
					let tile_x = tile % (image_width / tile_width);
					let tile_y = tile / (image_width / tile_width);

					let src_x = (tile_x * tile_width) as f32 / image_width as f32;
					let src_y = (tile_y * tile_height) as f32 / image_height as f32;
					let src_w = tile_width as f32 / image_width as f32;
					let src_h = tile_height as f32 / image_height as f32;

					let src_rect = graphics::Rect::new(src_x, src_y, src_w, src_h);
					let pos: mint::Point2<f32> = mint::Point2 {
						x: (x * tile_width) as f32,
						y: (y * tile_height) as f32,
					};

					let p = graphics::DrawParam {
						dest: pos,
						src: src_rect,
						..Default::default()
					};

					spritebatch.add(p);
				}
			}
		}

		Ok(spritebatch)
	}

	fn get_image(tileset: &Tileset) -> &str {
		&tileset.images[0].source[2..]
	}

	pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
		let param = graphics::DrawParam {
			..Default::default()
		};
		graphics::draw(ctx, &self.spritebatch, param)?;

		Ok(())
	}

	/// Get map grid dimensions (in count of tiles)
	pub fn get_grid_dimensions(&self) -> (u32, u32) {
		(self.map.width, self.map.height)
	}

	/// Get tile quad size (w = h = size)
	pub fn get_tile_size(&self) -> u32 {
		self.map.tilesets[0].tile_width
	}
}

pub struct LevelManager {}

impl LevelManager {
	pub fn load_level(ctx: &mut Context, name: &str) -> GameResult<Level> {
		let level_name = format!("/levels/{}.tmx", name);
		let file = open(ctx, level_name)?;

		tiled::parse(file)
			.map(|m| Level::new(ctx, name, m).unwrap())
			.map_err(|e| GameError::FilesystemError(e.description().to_string()))
	}
}
