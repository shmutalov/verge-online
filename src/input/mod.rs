pub trait Command {
    pub fn execute(&mut self);
}

impl Command {
    pub fn execute(&mut self) {}
}

pub struct MoveCommand {
    pub tile_x: u32,
    pub tile_y: u32,
}

impl Command for MoveCommand {
    pub fn execute(&mut self) {
        
    } 
}
