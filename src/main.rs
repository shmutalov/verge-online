extern crate ggez;

use ggez::conf::*;
use ggez::*;

pub mod managers;
pub mod states;

const WINDOW_WIDTH: f32 = 600.0;
const WINDOW_HEIGHT: f32 = 480.0;
const VERGE_ONLINE: &str = "Verge Online";

pub fn main() {
    let setup = WindowSetup::default()
        .title(VERGE_ONLINE)
        .vsync(false);

    let mode = WindowMode::default()
        .resizable(false)
        .fullscreen_type(FullscreenType::Windowed)
        .dimensions(WINDOW_WIDTH, WINDOW_HEIGHT)
        .min_dimensions(WINDOW_WIDTH, WINDOW_HEIGHT)
        .max_dimensions(WINDOW_WIDTH, WINDOW_HEIGHT);

    let (ctx, events_loop) = &mut ContextBuilder::new(VERGE_ONLINE, "shmutalov")
        .window_setup(setup)
        .window_mode(mode)
        .add_resource_path("resources")
        .build()
        .unwrap();

    let state = &mut states::GameState::new(ctx, "test_02").unwrap();

    event::run(ctx, events_loop, state).unwrap();
}
